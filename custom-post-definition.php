// Course Custom post
if (!function_exists('create_course_post_type')) {
    function create_course_post_type()
    {
        $labels = array(
            'name' => __( 'Cursos','framework'),
            'singular_name' => __( 'Curso','framework' ),
            'add_new' => __('Añadir nuevo','framework'),
            'add_new_item' => __('Añadir nuevo Curso','framework'),
            'edit_item' => __('Editar Curso','framework'),
            'new_item' => __('Nuevo Curso','framework'),
            'view_item' => __('Ver Curso','framework'),
            'search_items' => __('Buscar Curso','framework'),
            'not_found' =>  __('Curso no encontrado','framework'),
            'not_found_in_trash' => __('Curso no encontrado en la papelera','framework'),
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => 5,
			'taxonomies' => array('category', 'post_tag'),
            'supports' => array('title','editor','thumbnail'),
            'rewrite' => array(
                'with_front' => false,
                'slug'       => 'cursos'
            ),
        );

    register_post_type('course', $args);
    }
}

add_action('init', 'create_course_post_type');