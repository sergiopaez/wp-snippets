<?php
	$args = array(
		'post_type' => 'course',
		'posts_per_page' => -1,
	);
	
	$course_query = new WP_Query($args);
	
	if ($course_query->have_posts()) {
	while ($course_query->have_posts()) {
	$course_query->the_post();
?>

<div class="col-lg-6">
	<div class="course-item">
        <div class="course-img">

		</div>
        <div class="course-content">
            <h4 class="course-title"><?php the_title(); ?></h4>
            <?php if(get_field('course_temary')){ ?>
                <div class="course-temary">
                    <?php the_field('course_temary'); ?>
                </div>
            <?php } ?>
            <?php if(get_field('course_methodology')){ ?>
                <div class="course-methodology">
                    <?php the_field('course_methodology'); ?>
                </div>
            <?php } ?>
		</div>
    </div>
</div>

<?php
}
} else {
	nothing_found(__('No se encontraron casos de éxito', 'framework'));
}
wp_reset_query();
?>
